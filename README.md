# Utiliser IntelliJ pour les labos RES #

Dans cet exemple j’utiliserai le Labo-1.  
PS: je ne suis pas du tout un spécialiste de IntelliJ alors n'hésitez pas à fork et pull request des modifs xD
  

1. Forker le labo depuis le git du prof : [https://github.com/SoftEng-HEIGVD/Teaching-HEIGVD-RES-2017-Labo-01](Link URL)  

2. Ouvrir IntelliJ. Si vous avez un projet ouvert alors allez dans File->Close Project.  
3. La page de démarrage s'ouvre :![1.PNG](https://bitbucket.org/repo/ypKK8Ea/images/4133640610-1.PNG)  

4. Clickez sur Check out from Version Control : ![![2.PNG](https://bitbucket.org/repo/ypKK8Ea/images/738910280-2.PNG)](https://bitbucket.org/repo/ypKK8Ea/images/827279388-2.PNG)  

5. La fenêtre ci-dessus s'ouvre, il faut y entrer votre repo git `````git@github.com:**USERNAME**/Teaching-HEIGVD-RES-2017-Labo-01.git````` ainsi que le dossier dans lequel vous voulez faire le clone (chez moi par exemple `````...\Teaching-HEIGVD-RES-2016-LabBox\RES\repos`````).  

6. IntelliJ demande ensuite si l'on veut  qu'il crée un projet à partir des sources que l'on a checkout. La réponse est **oui** biensûr.  

7. On choisit ensuite d'importer le projet depuis un modèle externe **avec Maven** : ![3.PNG](https://bitbucket.org/repo/ypKK8Ea/images/1512824023-3.PNG)  

8. La fenêtre suivante s'ouvre :![5.PNG](https://bitbucket.org/repo/ypKK8Ea/images/678186143-5.PNG)  

9. A changer :  
- Search for projects recursively (YES).  
- Import Maven projects automatically (YES).  
- Create IntelliJ IDEA modules for aggregator projects (NO).  

10. Allez en bas a gauche dans `````Environment settings...````` et vérifiez que vous avez bien une version de Maven 3.3.9 ou plus. ![4.PNG](https://bitbucket.org/repo/ypKK8Ea/images/1586356703-4.PNG). Si tout est ok alors `````Next`````.  

11. Le programme propose d'importer une liste de snapshots (en l'occurence un seul). Vérifiez que le ou les snapshots soient sélectionnés et appuyez sur `````Next`````.  

12. Le programme demande de choisir un SDK : perso je n'ai que le 1.8 dont le home dir se trouve ici `````D:\Program Files\Java\jdk1.8.0_102`````. Appuyez sur `````Next`````.  

13. Le programme demande de nommer le projet et de définir les fichiers du projet. Perso je laisse tout et je met `````Next`````.

14. Vous voila enfin dans IntelliJ à proprement parlé. Encore 2 étapes et normalement tout roule! Dans l'onglet Project, vous devriez voir 2 dossiers LabXXApp-code et LabXXApp-test. ![6.PNG](https://bitbucket.org/repo/ypKK8Ea/images/2485125161-6.PNG)  

15. A cette étape, idéalement IntelliJ s'est débrouillé pour ne pas mettre des fichiers supplémentaires dans le git. Vous pouvez toujours verifier en allant dans la barre de menu, sur `````VCS->Commit Changes`````. Il devrait dire qu'il n'y a pas eu de changements!  

16. Allez sur `````Run->Edit Configurations`````. Ajoutez une config en cliquant sur le petit + vert et choisissez **Maven**. ![7.png](https://bitbucket.org/repo/ypKK8Ea/images/3747378125-7.png) 

17. Dans l'onglet Parameters, pour working directory, donnez le dossier contenant le code et les tests : `````./Teaching-HEIGVD-RES-2017-Labo-01/Lab01App-build````` . Comme ligne de commande ajoutez `````clean install`````. Et voila le tour est joué, il n'y a plus qu'a Run votre config maven. Ex : ![8.PNG](https://bitbucket.org/repo/ypKK8Ea/images/121392256-8.PNG)  

18. (Au cas ou) Si vous voulez pouvoir exécuter votre programme indépendamment des test alors créez vous une autre config **JAR Application** cette fois et configurez la comme dans l'image ci-dessous : ![9.PNG](https://bitbucket.org/repo/ypKK8Ea/images/4229010272-9.PNG)

19. Au cas où le point précédent ne fonctionne pas : créez une autre config __Application__ et choisissez :  
  * donnez-lui un nom (RUN_APP par exemple)  
  * Main class: `ch.heigvd.res.lab01.impl.Application`   
  * Program arguments: n'oubliez pas de mettre un nombre (j'ai choisi 50).   
  * Use classpath of module: `Lab01App-code`  

![tuto_IJ_run_app.PNG](https://bitbucket.org/repo/jgEggb7/images/1664485932-tuto_IJ_run_app.PNG)